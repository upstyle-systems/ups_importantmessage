<div class="alert alert-info">
	<p>{l s='If you would like to use displayAlert hook, you must insert code below in your template.' mod="ups_importantmessage"}</p>
	<p><strong>&lcub;hook h='displayAlert'&rcub;</strong></p>
</div>
