<div id="upsImpMes" {if $isPermanent}class="temp"{/if}>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				{$text}
				{if !$isPermanent}
					<a class="closer" onclick="accimpmes('{$hash}');">&times;</a>
				{/if}
			</div>
		</div>
	</div>
</div>
