<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_1e92b0e7212a0446cba6543b8dbbb94e'] = 'Důležité upozornění';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_c70c12279dab14bdffc4bd38bf8d8ecd'] = 'Důlěžité upozornění pro Vaše zákazníky';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_ae5e281e1040cc96589ac39a6bc7fd93'] = 'Opravdu chcete tento modul odinstalovat?';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_f4f70727dc34561dfde1a3c529b6205c'] = 'Nastavení';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_d2679b3aa8ca60a149888dd58c7d148b'] = 'Pozice zobrazení';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_cfe1b4b75fb8dc152b124f9305835a11'] = 'Vaše zpráva';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_5da618e8e4b89c66fe86e32cdafde142'] = 'Od';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_a0b34951c9a6e64b452611fdc5f7ada0'] = 'Pokud chcete zprávu okamžitě zobrazit a následně ji manuálně vypnout, odstraňte obsah tohoto pole';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_e12167aa0a7698e6ebc92b4ce3909b53'] = 'Do';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_db69ce2b253584f702745f210a658a0b'] = 'Permanentní?';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_db260449623318b7926d10203a240669'] = ' Pokud aktivujete tuto možnost, blok se bude zobrazovat permanentně. v opačném případě je k dispozici tlačítko [x] který se informační pruh zavře. ';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_a6474d7278efeed66401350b7e2a02a4'] = 'Ano';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_4dec99baa99738721da9c9b0c1a92498'] = 'Ne';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_c9cc8cce247e49bae79f15173ce97354'] = 'Uložit';
