<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_1e92b0e7212a0446cba6543b8dbbb94e'] = 'Dôležité upozornenie';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_6bae34beb1baa5d48d89a53f9d02aa98'] = 'Dôležité upozornenie pre váš web';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_a92cd64cebffd4954cfe1ad59f706101'] = 'Skutočne chcete odinštalovať modul?';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_f4f70727dc34561dfde1a3c529b6205c'] = 'Nastavenia';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_cfe1b4b75fb8dc152b124f9305835a11'] = 'Vaša správa';
$_MODULE['<{ups_importantmessage}prestashop>ups_importantmessage_c9cc8cce247e49bae79f15173ce97354'] = 'Uložiť';
