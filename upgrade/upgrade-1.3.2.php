<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_3_2($module)
{
    return $module->registerHook('displayHeader') && $module->unregisterHook('header');
}
