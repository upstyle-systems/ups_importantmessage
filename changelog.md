#### 2021-06-01
 * oprava podmínky zobrazení pruhu s/bez zadání data od - do

#### 2021-05-31
 * konfigurovatelné zavěšení
 * nastavení doby zobrazení od - do
 * přidání možnosti uzavření okna

#### 2018-08-02
 * vytvoření modulu
