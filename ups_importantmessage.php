<?php
if (!defined('_PS_VERSION_'))
    exit;

class ups_importantmessage extends Module
{
    public function __construct()
    {
        $this->name = 'ups_importantmessage';
        $this->tab = 'others';
        $this->version = '1.5.2';
        $this->author = 'UpStyle systems';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);

        parent::__construct();

        $this->displayName = $this->l('Important message');
        $this->description = $this->l('Important message for your customers');
        $this->confirmUninstall = $this->l('Are you really want to uninstall module?');
    }

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        return parent::install() &&
            $this->registerHook('displayHeader') &&
            $this->registerHook('displayAlert') &&
            $this->registerHook('displayBanner') &&
            $this->registerHook('displayHome') &&
            Configuration::updateValue('ups_importantmessage_perm', false) &&
            Configuration::updateValue('ups_importantmessage_from', false) &&
            Configuration::updateValue('ups_importantmessage_to', false) &&
            Configuration::updateValue('ups_importantmessage_default_hook', 'displayBanner');
    }


    public function uninstall()
    {
        return parent::uninstall() &&
            Configuration::deleteByName('ups_importantmessage') &&
            Configuration::deleteByName('ups_importantmessage_perm') &&
            Configuration::deleteByName('ups_importantmessage_from') &&
            Configuration::deleteByName('ups_importantmessage_to') &&
            Configuration::deleteByName('ups_importantmessage_default_hook');
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/front.css', 'all');
        $this->context->controller->addJS($this->_path . 'views/js/front.js');
    }

    public function renderFront($current_hook)
    {
        $hook = Configuration::get("ups_importantmessage_default_hook");
        $text = Configuration::get("ups_importantmessage", $this->context->language->id, $this->context->shop->id_shop_group, $this->context->shop->id);
        if ($hook != $current_hook || !$text)
            return false;
        $from = Configuration::get("ups_importantmessage_from");
        $to = Configuration::get("ups_importantmessage_to");
        if (
            (
                ($from and $to) and (date('Y-m-d') >= date('Y-m-d', strtotime($from)) and date('Y-m-d') <= date('Y-m-d', strtotime($to)))
            )
            or
            (
                !$from and !$to
            )
        ) {
            $type = Configuration::get("ups_importantmessage_type");
            $cookie = (bool)Configuration::get("ups_importantmessage_perm");
            $hash = hash('md5', $type . $text);
            if (isset($_COOKIE['impmes']) and $_COOKIE['impmes'] == $hash)
                return false;
            $this->context->smarty->assign(
                array(
                    'text' => $text,
                    'isPermanent' => $cookie,
                    'hash' => $hash,
                )
            );
            return $this->display(__FILE__, 'views/templates/hook/importantmessage.tpl');
        }
    }

    public function hookDisplayAlert()
    {
        return $this->renderFront('displayAlert');
    }

    public function hookDisplayBanner()
    {
        return $this->renderFront('displayBanner');
    }

    public function hookDisplayHome()
    {
        return $this->renderFront('displayHome');
    }

    public function getContent()
    {
        if (((bool)Tools::isSubmit('submitUpsImpMes')) == true) {
            $this->postProcess();
        }
        $this->context->smarty->assign('module_dir', $this->_path);
        return $this->renderForm();
    }

    protected function postProcess()
    {
        //message
        $messages = [];
        foreach(Language::getLanguages(true, $this->context->shop->id) as $lang) {
            $messages[$lang['id_lang']] = Tools::getValue('ups_importantmessage_'.$lang['id_lang']);
        }
        Configuration::updateValue('ups_importantmessage', $messages,true,$this->context->shop->id_shop_group, $this->context->shop->id);
        //config
        $form_values = $this->getConfigFormValues();
        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key), true);
        }
    }

    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitUpsImpMes';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        $this->context->smarty->assign('module_dir', $this->_path);
        $html = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/info.tpl');

        return $html . $helper->generateForm(array($this->getConfigForm()));
    }

    protected function getConfigFormValues()
    {
        $messages = [];
        foreach(Language::getLanguages(true, $this->context->shop->id) as $lang) {
            $messages[$lang['id_lang']] = Configuration::get('ups_importantmessage',$lang['id_lang'],$this->context->shop->id_shop_group, $this->context->shop->id);
        }
        return array(
            'ups_importantmessage' => $messages,
            'ups_importantmessage_perm' => Configuration::get('ups_importantmessage_perm'),
            'ups_importantmessage_from' => Configuration::get('ups_importantmessage_from'),
            'ups_importantmessage_to' => Configuration::get('ups_importantmessage_to'),
            'ups_importantmessage_default_hook' => Configuration::get('ups_importantmessage_default_hook'),
        );
    }

    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'name' => 'ups_importantmessage_default_hook',
                        'label' => $this->l('Default hook'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id_option' => 'displayAlert',
                                    'name' => 'displayAlert'
                                ),
                                array(
                                    'id_option' => 'displayBanner',
                                    'name' => 'displayBanner'
                                ),
                                array(
                                    'id_option' => 'displayHome',
                                    'name' => 'displayHome'
                                ),
                            ),
                            'id' => 'id_option',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'col' => 5,
                        'rows' => 5,
                        'type' => 'textarea',
                        'name' => 'ups_importantmessage',
                        'label' => $this->l('Your message'),
                        'lang' => true,
                    ),
                    array(
                        'type' => 'date',
                        'name' => 'ups_importantmessage_from',
                        'label' => $this->l('From'),
                        'desc' => $this->l('If you would like to display it imediatelly and hide it manually, delete content of this field.'),
                    ),
                    array(
                        'type' => 'date',
                        'name' => 'ups_importantmessage_to',
                        'label' => $this->l('To'),
                        'desc' => $this->l('If you would like to display it imediatelly and hide it manually, delete content of this field.'),
                    ),
                    array(
                        'type' => 'switch',
                        'name' => 'ups_importantmessage_perm',
                        'label' => $this->l('Permanent?'),
                        'desc' => $this->l('If you activate this option, block will be visible permanently, without close option. Otherwise, there is an close button for customers.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Ano')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Ne')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

}
